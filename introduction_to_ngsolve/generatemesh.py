
#%%

from ngsolve import *
## choose this option to draw in interactive window (VSCode)
from ngsolve.webgui import Draw
## choose this option for a separate gui window
# import netgen.gui


from ngsolve.meshes import MakeStructured2DMesh
import netgen.geom2d as ng2d

import numpy as np

import matplotlib.pyplot as plt

#%%
length= 100
width = 50

#%%
## how to generate a structured mesh
def transformation(x, y): return (length/2*x, width/2*y)

mesh = MakeStructured2DMesh(quads=True, nx=2, ny=1, mapping=transformation)
Draw(mesh)
print(mesh.GetBoundaries())
#%%
## how to generate an unstructured triangular mesh

geo = ng2d.SplineGeometry()

pnt1 = geo.AppendPoint(0,0)
pnt2 = geo.AppendPoint(length/2,0)
pnt3 = geo.AppendPoint(length/2,width/2)
pnt4 = geo.AppendPoint(0,width/2)

geo.Append(["line", pnt1, pnt2], leftdomain=1, rightdomain=0, bc="bottom")
geo.Append(["line", pnt2, pnt3], leftdomain=1, rightdomain=0, bc="right")
geo.Append(["line", pnt3, pnt4], leftdomain=1, rightdomain=0, bc="top")
geo.Append(["line", pnt4, pnt1], leftdomain=1, rightdomain=0, bc="left")

mesh = Mesh(geo.GenerateMesh(maxh=width/2))

Draw(mesh)
print(mesh.GetBoundaries())
#%%
