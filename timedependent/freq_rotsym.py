#%%

from ngsolve import *
# from netgen.geom2d import *
from ngsolve.meshes import MakeStructured2DMesh

import netgen.gui

import numpy as np

#%%

## compute in mm - scale = 1e3
scale = 1e3
radius = 30*1e-3*scale
height = 30*1e-3*scale

def transformation(x, y): return (radius*x, height*y)
# def transformation(x, y): return (radius*(1-(1-x)**2), height*y**2)

mesh = MakeStructured2DMesh(quads=True, nx=10, ny=10, mapping=transformation)
Draw(mesh)

## rescale Young modulus when computing in mm
## Elastic modulus
Emodul = 2e6/scale/scale  ## N/mm^2
## Poisson ratio
nu = 0.3
## rescale density when computing in mm
rho = 1e3*scale**(-4)

mu = Emodul/2/(1+nu)
lam_planestrain = Emodul*nu/(1+nu)/(1-2*nu)
lam_planestress = Emodul*nu/(1-nu*nu)

tipload = 3500*scale**(-2)


#%%

V = VectorH1(mesh, order=2, dirichlet="bottom", dirichletx="left")
u = GridFunction(V)

#%%

def Grad3D(U):
    DURDR = grad(U)[0,0]
    DURDZ = grad(U)[0,1]
    DUZDR = grad(U)[1,0]
    DUZDZ = grad(U)[1,1]
    
    G = IfPos(x-1e-4, CoefficientFunction((DURDR, 0, DURDZ, 0, 1/(x)*U[0], 0, DUZDR, 0, DUZDZ), dims=(3,3)),
    CoefficientFunction((DURDR, 0, DURDZ, 0, DURDR, 0, DUZDR, 0, DUZDZ), dims=(3,3)) )

    return G

#%%

u_ = V.TrialFunction()
deltau_ = V.TestFunction()

strain_ = Sym(Grad3D(u_))
deltastrain_ = Sym(Grad3D(deltau_))

k = BilinearForm(V)
m = BilinearForm(V)
f = LinearForm(V)

k += SymbolicBFI((2*mu* InnerProduct(strain_, deltastrain_) + lam_planestrain*Trace(strain_)*Trace(deltastrain_))*2*np.pi*x)

m += SymbolicBFI(rho*InnerProduct(u_, deltau_)*2*np.pi*x)

f += SymbolicLFI( tipload*deltau_[1]*2*np.pi*x, definedon=mesh.Boundaries("top"))

k.AssembleLinearization(u.vec)
m.AssembleLinearization(u.vec)
f.Assemble()


sysmat = m.mat.CreateMatrix()
sysvec = f.vec.CreateVector()

#%%

freqlist = np.linspace(0,600,101)
defllist = np.zeros(101)

for (i,freq) in enumerate(freqlist):
    print(f"freq {freq}, {i}/100")

    omega = 2*np.pi*freq

    sysmat.AsVector().data = k.mat.AsVector() - omega*omega*m.mat.AsVector()

    invsysmat = sysmat.Inverse(V.FreeDofs(), inverse="umfpack")

    u.vec.data = invsysmat*f.vec

    defllist[i] = u(mesh(0,height))[1]

print(defllist)
#%%

import matplotlib.pyplot as plt
plt.semilogy(freqlist, np.abs(defllist))

#%%


#%%


## inverse iteration

h = u.vec.CreateVector()
h.SetRandom()
u.vec.data = Projector(V.FreeDofs(), True) * h
Draw(u, mesh, "u")

## macos
inversek = k.mat.Inverse(V.FreeDofs(), inverse="umfpack")
## windows
inversek = k.mat.Inverse(V.FreeDofs(), inverse="pardiso")

for i in range(20):
    h.data = m.mat * u.vec

    u.vec.data = inversek * h

    normu = u.vec.Norm()
    u.vec.data *= 1/normu

    lambdai = InnerProduct(u.vec, k.mat*u.vec)/InnerProduct(u.vec, m.mat*u.vec)

    print("lambda = ", lambdai, ", omega = ", np.sqrt(lambdai), ", f = ", np.sqrt(lambdai)/2/np.pi)
    

# %%
