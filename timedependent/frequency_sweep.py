#%%

from ngsolve import *
from netgen.geom2d import *

import netgen.gui

import numpy as np

#%%

## compute in mm - scale = 1e3
scale = 1e3
h = 0.01*scale
l = 0.1*scale

## rescale Young modulus when computing in mm
Emodul = 2e7*scale**(-2)
nu = 0.3
## rescale density when computing in mm
rho = 1e3*scale**(-4)

mu = Emodul/2/(1-nu)
lam_planestrain = Emodul*nu/(1-nu)/(1-2*nu)
lam_planestress = Emodul*nu/(1-nu*nu)

tipload = 100*scale**(-2)

#%%

geo = SplineGeometry()

pnt1 = geo.AppendPoint(0,-h/2)
pnt2 = geo.AppendPoint(l,-h/2)
pnt3 = geo.AppendPoint(l,h/2)
pnt4 = geo.AppendPoint(0,h/2)

geo.Append(["line", pnt1, pnt2], leftdomain=1, rightdomain=0, bc="bottom")
geo.Append(["line", pnt2, pnt3], leftdomain=1, rightdomain=0, bc="right")
geo.Append(["line", pnt3, pnt4], leftdomain=1, rightdomain=0, bc="top")
geo.Append(["line", pnt4, pnt1], leftdomain=1, rightdomain=0, bc="left")

mesh = Mesh(geo.GenerateMesh(maxh=h/6))

Draw(mesh)
print(mesh.GetBoundaries())

#%%

V = VectorH1(mesh, order=2, dirichlet="left")
u = GridFunction(V)

#%%

u_ = V.TrialFunction()
deltau_ = V.TestFunction()

strain_ = Sym(Grad(u_))
deltastrain_ = Sym(Grad(deltau_))

k_planestrain = BilinearForm(V)
k_planestress = BilinearForm(V)
m = BilinearForm(V)
f = LinearForm(V)

k_planestrain += SymbolicBFI(2*mu* InnerProduct(strain_, deltastrain_) + lam_planestrain*Trace(strain_)*Trace(deltastrain_))
k_planestress += SymbolicBFI(2*mu* InnerProduct(strain_, deltastrain_) + lam_planestress*Trace(strain_)*Trace(deltastrain_))

m += SymbolicBFI(rho*InnerProduct(u_, deltau_))

f += SymbolicLFI( tipload*deltau_[1], definedon=mesh.Boundaries("right"))

k_planestrain.AssembleLinearization(u.vec)
k_planestress.AssembleLinearization(u.vec)
m.AssembleLinearization(u.vec)
f.Assemble()


sysmat = m.mat.CreateMatrix()
sysvec = f.vec.CreateVector()

#%%

freqlist = np.linspace(0,1000,101)
defllist = np.zeros(101)

for (i,freq) in enumerate(freqlist):
    print(f"freq {freq}, {i}/100")

    omega = 2*np.pi*freq

    sysmat.AsVector().data = k_planestrain.mat.AsVector() - omega*omega*m.mat.AsVector()

    invsysmat = sysmat.Inverse(V.FreeDofs(), inverse="pardiso")

    u.vec.data = invsysmat*f.vec

    defllist[i] = u(mesh(l,0))[1]

#%%

import matplotlib.pyplot as plt
plt.semilogy(freqlist, np.abs(defllist))

#%%

eigenfreq = []
for i in range(len(freqlist)-2):
    if np.abs(defllist[i]) > np.abs(defllist[i-1]) and np.abs(defllist[i]) > np.abs(defllist[i+1]):
        eigenfreq.append(freqlist[i])

print(eigenfreq)
    
# %%

eigenfunclist = []

for (i, freq) in enumerate(eigenfreq):
    ue = GridFunction(V)
    eigenfunclist.append(ue)

    omega = 2*np.pi*freq

    sysmat.AsVector().data = k_planestrain.mat.AsVector() - omega*omega*m.mat.AsVector()

    invsysmat = sysmat.Inverse(V.FreeDofs(), inverse="pardiso")

    ue.vec.data = invsysmat*f.vec

    Draw(ue, mesh, f"u{i}")

#%%